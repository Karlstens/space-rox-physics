{
    "id": "05f39ca9-f35e-449d-8222-4357e2762a23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aab3ae0c-d60c-4c1f-b0f6-b542d7fc85f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f39ca9-f35e-449d-8222-4357e2762a23",
            "compositeImage": {
                "id": "ddd7049d-4a87-4670-9224-15ec478c51a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab3ae0c-d60c-4c1f-b0f6-b542d7fc85f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2dd3459-d29e-4d5a-8587-901527513f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab3ae0c-d60c-4c1f-b0f6-b542d7fc85f4",
                    "LayerId": "ada88206-49be-46ef-8536-015d71d2b665"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ada88206-49be-46ef-8536-015d71d2b665",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f39ca9-f35e-449d-8222-4357e2762a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 4
}
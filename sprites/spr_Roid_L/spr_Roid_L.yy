{
    "id": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Roid_L",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "98fc0256-0b0b-4752-91e6-226a05c04211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "ba4c46c3-78d9-4749-9bd2-6dc40f2ce3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fc0256-0b0b-4752-91e6-226a05c04211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d653f3c-2d31-451a-af52-86e57808c92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fc0256-0b0b-4752-91e6-226a05c04211",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        },
        {
            "id": "e4a9072e-18f8-4b9e-af95-96a635461d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "7f3817e1-7701-4e37-b292-cbfc4c090c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a9072e-18f8-4b9e-af95-96a635461d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ad8b03-4f37-46c4-98e2-8ee723786dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a9072e-18f8-4b9e-af95-96a635461d5d",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        },
        {
            "id": "4dd51524-c9ca-4e52-8566-72624d957d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "cd4f17f6-1512-4540-a35b-389ebe951041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd51524-c9ca-4e52-8566-72624d957d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8f3d88-89c6-4509-a513-609ba2afd3f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd51524-c9ca-4e52-8566-72624d957d00",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        },
        {
            "id": "4b2fed35-5c90-4f4a-97f4-76e3be882c45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "1c3fb261-1ddf-4bd1-b131-ba551d2b86bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2fed35-5c90-4f4a-97f4-76e3be882c45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e049ebb8-e5da-4f8d-985b-0817ebcc565d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2fed35-5c90-4f4a-97f4-76e3be882c45",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        },
        {
            "id": "bd8698cb-b82b-48fb-b3b6-627f4d6fa652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "compositeImage": {
                "id": "e6d95310-0861-4cb6-8335-4a358e494c2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd8698cb-b82b-48fb-b3b6-627f4d6fa652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862f2c86-34da-4865-8d90-6cc09f9bb559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd8698cb-b82b-48fb-b3b6-627f4d6fa652",
                    "LayerId": "ff771916-b252-470d-8e91-65abc40d4e17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff771916-b252-470d-8e91-65abc40d4e17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}
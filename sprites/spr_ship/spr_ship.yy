{
    "id": "aa37d77e-b78d-45b2-8bfe-61d9022698fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": -1,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90bcd4af-7cc7-48ce-9479-9fd1a0fc58e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa37d77e-b78d-45b2-8bfe-61d9022698fe",
            "compositeImage": {
                "id": "f632fdf0-d6a5-4ee3-83a5-df2308a63840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90bcd4af-7cc7-48ce-9479-9fd1a0fc58e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462643ed-52b9-4bbc-bd5a-66ea8257e250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90bcd4af-7cc7-48ce-9479-9fd1a0fc58e7",
                    "LayerId": "7f3e124a-a2f2-4235-a87c-8a51600f41c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f3e124a-a2f2-4235-a87c-8a51600f41c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa37d77e-b78d-45b2-8bfe-61d9022698fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
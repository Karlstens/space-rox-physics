{
    "id": "7fa5f6d0-2b95-4f32-850f-30750799c2b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pwr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00426f11-36d9-48ae-830b-18a16e86c97a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa5f6d0-2b95-4f32-850f-30750799c2b6",
            "compositeImage": {
                "id": "18fe87a1-859e-4e85-86d9-eb5261aef9a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00426f11-36d9-48ae-830b-18a16e86c97a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd52cf90-342a-4125-95ef-91f1f6b15589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00426f11-36d9-48ae-830b-18a16e86c97a",
                    "LayerId": "c07a1d25-939c-430a-ad10-cae0d69ec1df"
                }
            ]
        },
        {
            "id": "d6ae3406-b50c-48df-b2fa-a618e6f6a096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa5f6d0-2b95-4f32-850f-30750799c2b6",
            "compositeImage": {
                "id": "d554eb0d-4bf1-4420-bcab-0cae6e211350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ae3406-b50c-48df-b2fa-a618e6f6a096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33264d4-e696-44c7-96bd-9840be714cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ae3406-b50c-48df-b2fa-a618e6f6a096",
                    "LayerId": "c07a1d25-939c-430a-ad10-cae0d69ec1df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c07a1d25-939c-430a-ad10-cae0d69ec1df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fa5f6d0-2b95-4f32-850f-30750799c2b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
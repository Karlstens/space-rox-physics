{
    "id": "0b7e9c49-b1b6-43a1-a11f-fe4c069696cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_thrust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "854a8e19-9ee0-47d8-af23-b4d538a638d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7e9c49-b1b6-43a1-a11f-fe4c069696cb",
            "compositeImage": {
                "id": "c4b0e655-cf15-4b59-863a-2595cca20b10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854a8e19-9ee0-47d8-af23-b4d538a638d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c719496-2a3e-4bc9-9434-cc65632afc0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854a8e19-9ee0-47d8-af23-b4d538a638d2",
                    "LayerId": "bba8de28-e133-41f4-b60e-d04af845721e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "bba8de28-e133-41f4-b60e-d04af845721e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b7e9c49-b1b6-43a1-a11f-fe4c069696cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}
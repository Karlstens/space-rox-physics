///scr_place_roid(obj_count, obj_to_create)
///@param obj_count
///@param obj_to_create

var _x = 0;
var _y = 0;
var angle = 0 ;
var radius = sqrt(sqr(sprite_width/4) + sqr(sprite_width/4));


for 
	(
	angle = phy_rotation+45; //starting point - top right corner. 
	angle < phy_rotation+360; //angle available for placements
	angle += (360 / argument0); //evenly distribute total placements
	)
	{	
	_x = x+(radius*cos(degtorad(angle)));
	_y = y+(radius*sin(degtorad(angle)));

	with (instance_create_layer(_x,_y,"Layer_Roids",argument1))
		{
			phy_speed_x = other.phy_speed_x;
			phy_speed_y = other.phy_speed_y;
			physics_apply_local_impulse(0, 0, 0,sprite_width*(other.phy_angular_velocity/500)); 
		
		} 
}

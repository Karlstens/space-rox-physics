///scr_create_debris(debris_count, debris_object, debris_origin)
///@param debris_count
///@param debris_object
///@param debris_origin_0-3
//Need a switch for X,y Origin of debris, either for parentobectXY, OtherObjectXY, or Other.CollisionXY

//show_message("scr blank: " + string(phy_collision_x) + " " + string(phy_collision_y))
//show_message("scr self: " + string(self.phy_collision_x) + " " + string(self.phy_collision_y))
//show_message("scr other: " + string(other.phy_collision_x) + " " + string(other.phy_collision_y))

var PosX;
var PosY;

switch (argument2) //select origin of debris either from collision XY or ParentXY or OtherXY
{
	case 0: 
		PosX = self.phy_collision_x;
		PosY = self.phy_collision_y;
	break;
	case 1:
		PosX = self.phy_position_x;
		PosY = self.phy_position_y;
	break;
	case 2:
		PosX = other.phy_position_x;
		PosY = other.phy_position_y;
	break;
	default:
	break;

}


repeat(argument0)		//debris	
{
with (instance_create_layer(PosX,PosY,"Layer_Roids",argument1))
	{
		phy_rotation = irandom_range(0,359);
		phy_speed_x = other.phy_speed_x;
		phy_speed_y = other.phy_speed_y;
		physics_apply_local_impulse(0, 0.005, 0,0.005);
	}
}

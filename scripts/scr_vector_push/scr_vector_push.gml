///scr_vector_push()

var yy = 0; with(obj_Roid_Master)
{
		var phy_rot_cache =  phy_rotation;
		phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
		physics_apply_local_force(0,0,1000-point_distance(obj_Player.phy_position_x,obj_Player.phy_position_y,phy_position_x,phy_position_y),0);
		phy_rotation = phy_rot_cache;
	yy++;
}

var yy = 0; with(obj_Debris)
{
		var phy_rot_cache =  phy_rotation;
		phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
		physics_apply_local_force(0,0,0.5,0);
		phy_rotation = phy_rot_cache;
	yy++;
}

var yy = 0; with(obj_Thrust)
{
		var phy_rot_cache =  phy_rotation;
		phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
		physics_apply_local_force(0,0,0.1,0);
		phy_rotation = phy_rot_cache;
	yy++;
}

var yy = 0; with(obj_bullet)
{
		var phy_rot_cache =  phy_rotation;
		phy_rotation = point_direction(obj_Player.x, obj_Player.y,phy_position_x, phy_position_y )*-1;
		physics_apply_local_force(0,0,100,0);
		phy_rotation = phy_rot_cache;
	yy++;
}

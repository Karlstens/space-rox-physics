{
    "id": "3cb20967-820f-4988-9f08-32ea34ec9e21",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Roid_S",
    "eventList": [
        {
            "id": "bd5f4e7c-ee06-4263-aea4-fb0a7bfe2980",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3cb20967-820f-4988-9f08-32ea34ec9e21"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f2191131-ba11-4f84-b804-8934619d17fe",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.5,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 0.5,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "4b711c73-8523-4d00-9af7-bfdfb198ce76",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        },
        {
            "id": "352d69b2-8543-4d50-93ee-777e8f412c21",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "ba8b7ab5-51da-4f93-9992-c893d4a1a9c1",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1.0",
            "varName": "Impact_Speed",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "3f7e6240-ed0c-4c08-aed2-1cc2f4fc3963",
    "visible": true
}
/// @desc Roid Boot Sequence

hitpoints = 0 // similar to demerit points
maxhealth = ceil((sprite_width/16) + 1)
damage = ceil((sprite_width/16))

image_index = hitpoints;

phy_rotation = (irandom(359));
physics_apply_local_impulse(0, 0, irandom(1)/10,0); 
physics_apply_angular_impulse(irandom_range(1,10)/10);
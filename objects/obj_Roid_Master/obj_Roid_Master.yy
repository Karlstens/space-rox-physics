{
    "id": "f2191131-ba11-4f84-b804-8934619d17fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Roid_Master",
    "eventList": [
        {
            "id": "e3c127f0-0efa-4fab-bc4c-3cf59ee396c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "f2191131-ba11-4f84-b804-8934619d17fe"
        },
        {
            "id": "45c8524f-e5a8-4326-b2bf-c4ad77f1a733",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f2191131-ba11-4f84-b804-8934619d17fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "666f8ea8-d963-4cc2-9d92-a24483c99fba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "aee69ea8-cfa0-42c5-9212-ce0178653f2c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "a257e093-e8ce-4f1f-aa67-51a11fb39fd7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "d989e3cc-51b3-4c2e-b82f-63703f93f989",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "b95dfcdc-4c14-4c12-8205-7743cf579e71",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Control",
    "eventList": [
        {
            "id": "4d34822b-330b-451f-bc9f-af1f5f5c94ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "b95dfcdc-4c14-4c12-8205-7743cf579e71"
        },
        {
            "id": "d706334f-070b-4c42-8c04-cf1c8dde14b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b95dfcdc-4c14-4c12-8205-7743cf579e71"
        },
        {
            "id": "5894f524-a425-40b2-9bae-08b4950684e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b95dfcdc-4c14-4c12-8205-7743cf579e71"
        },
        {
            "id": "bd1f54fe-5415-445b-aac3-24b5bbc165b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b95dfcdc-4c14-4c12-8205-7743cf579e71"
        },
        {
            "id": "4fd1f497-2dec-42e9-ae10-de5bab7ee30f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "b95dfcdc-4c14-4c12-8205-7743cf579e71"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
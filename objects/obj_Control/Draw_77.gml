/// @desc GIF record
if (keyboard_check_pressed(ord("G")))
{
	gifrecord = !gifrecord;
	
	if (gifrecord) //start record
	{	
		gif = gif_open(room_width, room_height);
	}
	
	else
	{
		gif_save(gif, "capture.gif");
	}
		
}

if (gifrecord)
{
	gif_add_surface(gif, application_surface,2);
}
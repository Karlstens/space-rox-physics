/// @desc New Asteroids

var _object = obj_Roid_L ;
var _object_offset = sprite_get_width(object_get_sprite(_object))


//show_message(string(sprite_get_width(_object)))

if(choose(0,1)) //Choose left/right or top/bottom
{ //1 left/right
	var xx = choose(0-_object_offset,room_width+_object_offset);
	var yy = irandom_range(0,room_height+sprite_get_width(_object)-(sprite_get_width(_object)/2));

}else 
{ //0 top/bottom
	var xx = irandom_range(0,room_width+sprite_get_width(_object)-(sprite_get_width(_object)/2));
	var yy = choose(0-_object_offset,room_height+_object_offset);
}

var roid = instance_create_layer(xx,yy,"Layer_Roids", _object)
			roid.phy_rotation = irandom_range(0,359);
			roid.phy_speed_x += (irandom_range(1,6))/10;
			roid.phy_speed_y += (irandom_range(1,6))/10;

alarm[1] = instance_number(_object) * room_speed; //roid spawn

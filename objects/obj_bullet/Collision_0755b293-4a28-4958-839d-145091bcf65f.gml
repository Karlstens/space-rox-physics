/// @desc Bullet Collision
// audio_play_sound(roidsmashL,1,0)

scr_score();
scr_create_debris(10,obj_Debris,0);

with(other)
	{
	hitpoints += 1;
	image_index = hitpoints;
	if (hitpoints >= 3)
		{
		scr_place_roid(4,obj_Roid_M);
		instance_destroy();
		}
	}

hitpoints += 1
if hitpoints >= 2
{
	instance_destroy(self);
}
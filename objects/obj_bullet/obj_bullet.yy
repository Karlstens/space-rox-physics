{
    "id": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "1880bb1f-39c8-43c0-939e-fe285178f592",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        },
        {
            "id": "d7b9a802-9c8d-4012-8c52-077a405d942d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        },
        {
            "id": "c096c2f6-dff7-41b5-951d-84cce3391289",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a8ab7652-cfe4-4385-8781-ad051cb75cce",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        },
        {
            "id": "0755b293-4a28-4958-839d-145091bcf65f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b9712e10-c3a7-4257-9df8-0b779666ec9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        },
        {
            "id": "12afa8e3-71c6-4184-b13f-1857ba55b605",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cb20967-820f-4988-9f08-32ea34ec9e21",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        },
        {
            "id": "9468b5a1-9a94-4ab6-833b-a19825f60537",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b783ecb2-ae16-4c6d-bc26-7edfd0a8c31f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "5424f348-e744-4e54-b319-df9c4c64f2f2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "90d266fb-8f1d-4179-876b-9d3ee3e179d8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 0
        },
        {
            "id": "9e770963-0083-4976-a195-48902532d78e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 4
        },
        {
            "id": "280fb49c-698d-4fdc-8797-ecb7132cd61f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05f39ca9-f35e-449d-8222-4357e2762a23",
    "visible": true
}
/// @desc Rare Case Bullet Leak

scr_edge_instance_destroy();

//Bullets expire after 2.5 seconds
if time_stamp + 2500000 < get_timer()
	{
	scr_create_debris(10,obj_Debris,1);
	instance_destroy();
	}
	
{
    "id": "27b0b881-8c63-4a8e-8d4e-4d8bef6ecc00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Thrust",
    "eventList": [
        {
            "id": "dbc7fac0-f04d-459c-90a5-4cda5e7afc3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27b0b881-8c63-4a8e-8d4e-4d8bef6ecc00"
        },
        {
            "id": "fbfb4cb7-8752-4942-b654-1b794cb7d166",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27b0b881-8c63-4a8e-8d4e-4d8bef6ecc00"
        },
        {
            "id": "163f963b-5bb7-458e-81de-1aebc6b5f5fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "27b0b881-8c63-4a8e-8d4e-4d8bef6ecc00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.001,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "d24fa753-6a02-4b59-81c4-a3c00fe11c63",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bddd4c4c-7214-427e-9849-6abb9d854fcc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 0
        },
        {
            "id": "3fd38595-5e98-4ad3-8c45-cd8599b78bf3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 4
        },
        {
            "id": "773465d0-43cd-466f-9cfb-14c565170ac6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b7e9c49-b1b6-43a1-a11f-fe4c069696cb",
    "visible": true
}
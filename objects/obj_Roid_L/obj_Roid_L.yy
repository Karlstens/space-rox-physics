{
    "id": "b9712e10-c3a7-4257-9df8-0b779666ec9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Roid_L",
    "eventList": [
        {
            "id": "2fb3ad85-98c3-46db-93c2-4515c74eedb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b9712e10-c3a7-4257-9df8-0b779666ec9c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9712e10-c3a7-4257-9df8-0b779666ec9c"
        },
        {
            "id": "e3692e3a-f6e7-443e-b124-8ecc47b3eaea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a8ab7652-cfe4-4385-8781-ad051cb75cce",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9712e10-c3a7-4257-9df8-0b779666ec9c"
        },
        {
            "id": "99711651-f086-4bfa-922e-e8ee142302a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "90da8095-774e-4686-8f2f-b1f5ca2c3255",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9712e10-c3a7-4257-9df8-0b779666ec9c"
        },
        {
            "id": "c6571da1-7290-4a02-8a47-fbe23bfe8033",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cb20967-820f-4988-9f08-32ea34ec9e21",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9712e10-c3a7-4257-9df8-0b779666ec9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f2191131-ba11-4f84-b804-8934619d17fe",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "31cf3410-0de4-4e5f-b6a8-081949db3c93",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14.9252357,
            "y": 5.476638
        },
        {
            "id": "82300b51-cff8-48f9-9311-2d16d9ab743c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 47.30841,
            "y": 5.943928
        },
        {
            "id": "1dfa24e8-e745-4ca0-b223-8aec9086e95e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 59.4859848,
            "y": 13.04673
        },
        {
            "id": "ee07a619-b1b9-477c-ba6f-d814f2af2cb2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 60.2056046,
            "y": 47.6542053
        },
        {
            "id": "99cc76da-6572-4cf5-9502-044391ab1341",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 52.1869125,
            "y": 61.3644867
        },
        {
            "id": "576c64f1-2dcd-440d-bfea-1cf933b782ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 5,
            "y": 62
        },
        {
            "id": "c4df9880-82a1-4ffa-b6aa-5aa9ff4121dd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 25
        },
        {
            "id": "2b1f7074-da8e-4dd3-b058-498f53feb375",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 6
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9623fe8e-d5f0-42c4-97b1-4d9ec519deb2",
    "visible": true
}
/// @desc small roid impact

//low impact
scr_create_debris(2,obj_Debris,0);
var Impact_Speed = 1;
var Smash_Speed = sqrt(abs(power(phy_speed_x - other.phy_speed_x,2)) + abs(power(phy_speed_y - other.phy_speed_y,2)));;

//high impact

if Smash_Speed > Impact_Speed
{
	hitpoints += other.damage;
	if (hitpoints >= maxhealth)
		{
		scr_place_roid(4,obj_Roid_M);
		instance_destroy(self);
		}
	image_index = hitpoints;	
	with(other) //small roid
	{
		hitpoints += other.damage
		if (hitpoints >= maxhealth)
		{
			scr_create_debris(10,obj_Debris,0);	
			instance_destroy();
		}
	image_index = hitpoints;
	}
}
/// @desc medium roid impact
scr_create_debris(2,obj_Debris,0);

var Impact_Speed = 1;
var Smash_Speed = sqrt(abs(power(phy_speed_x - other.phy_speed_x,2)) + abs(power(phy_speed_y - other.phy_speed_y,2)));;

//

if Smash_Speed > Impact_Speed
	{
	scr_create_debris(10,obj_Debris,0);
	
	hitpoints += other.damage;
	image_index = hitpoints;
	if (hitpoints >= maxhealth)
		{
		scr_place_roid(4,obj_Roid_M); //large breaks into 4 medium
		instance_destroy(self);
		}
		
	with(other)
	{
	hitpoints += other.damage
	image_index = hitpoints;
	if (hitpoints >= maxhealth)
		{
		scr_place_roid(choose(3,4),obj_Roid_S);//medium breaks into 3 or 4 small
		instance_destroy(self);
		}
	}
}

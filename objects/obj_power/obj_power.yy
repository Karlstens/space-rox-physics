{
    "id": "dffa9801-92d3-4b95-ab5a-a3f10cd32a79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_power",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 0,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ec868aaa-4151-4487-b229-3847f74c9037",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "c4278737-fe9b-419c-b2ad-0091dd309214",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "8c8bdb2e-7f1d-441d-9b49-cb01b07cfe4d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "3086c65e-5543-4b42-9970-63b7ab96e181",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7fa5f6d0-2b95-4f32-850f-30750799c2b6",
    "visible": true
}
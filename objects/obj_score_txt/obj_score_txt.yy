{
    "id": "3aec8b16-33d5-4d09-af94-f52b5cbc950b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score_txt",
    "eventList": [
        {
            "id": "2eb47a3f-3f1f-47f3-8908-2576ae307856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aec8b16-33d5-4d09-af94-f52b5cbc950b"
        },
        {
            "id": "0a8ce76e-5da9-4468-b54c-96d83ff1869b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3aec8b16-33d5-4d09-af94-f52b5cbc950b"
        },
        {
            "id": "59fc05ad-6949-4d36-b8d0-f1afe1fa9912",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3aec8b16-33d5-4d09-af94-f52b5cbc950b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
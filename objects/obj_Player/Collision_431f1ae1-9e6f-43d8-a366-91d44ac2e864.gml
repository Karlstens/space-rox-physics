/// @desc Ship crash into small roid
	
	scr_create_debris(4,obj_Debris,0)

var Impact_Speed = 3
var Smash_Speed = sqrt(power(phy_speed_x - other.phy_speed_x,2) + power(phy_speed_y - other.phy_speed_y,2))

if Smash_Speed > Impact_Speed
{
	scr_create_debris(10,obj_Debris,0);
	with(other)
	{
		instance_destroy();
	}
}
{
    "id": "90da8095-774e-4686-8f2f-b1f5ca2c3255",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "d316d89a-bb55-43e3-9a3a-008da73f9c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        },
        {
            "id": "431f1ae1-9e6f-43d8-a366-91d44ac2e864",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cb20967-820f-4988-9f08-32ea34ec9e21",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        },
        {
            "id": "b57c868f-7f6a-44ba-8af2-dc97500681ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dffa9801-92d3-4b95-ab5a-a3f10cd32a79",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        },
        {
            "id": "7e8b8a81-5833-49a8-b3c6-f6c49b51650d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 5,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        },
        {
            "id": "1bbe9c95-25c1-442b-91aa-7580c76823e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        },
        {
            "id": "ea25bfbe-92a5-401e-9469-e57ae483ab27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90da8095-774e-4686-8f2f-b1f5ca2c3255"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "persistent": false,
    "physicsAngularDamping": 0.5,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "bca09984-6707-4351-83ea-4b0188d41510",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31.75,
            "y": 16
        },
        {
            "id": "184f5123-c5f2-447a-b166-d884e47d577f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3.75,
            "y": 30
        },
        {
            "id": "3566b8f3-7d5e-411c-99b1-6c52bbd8fa17",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2.75,
            "y": 2
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "66db74ee-8fb9-4a3d-932d-467580d9c4e9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1",
            "varName": "thruster_engage",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "aa37d77e-b78d-45b2-8bfe-61d9022698fe",
    "visible": true
}
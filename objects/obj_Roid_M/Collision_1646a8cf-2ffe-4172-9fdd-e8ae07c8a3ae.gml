/// @desc medium roid impact
scr_create_debris(2,obj_Debris,0);

var Impact_Speed = 1.5;
var Smash_Speed = sqrt(abs(power(phy_speed_x - other.phy_speed_x,2)) + abs(power(phy_speed_y - other.phy_speed_y,2)));

if Smash_Speed > Impact_Speed
	{
	hitpoints += other.damage;
	if (hitpoints >= maxhealth)
		{
		scr_place_roid(choose(3,4),obj_Roid_S);
		instance_destroy(self)
		}
	image_index = hitpoints;
	scr_create_debris(10,obj_Debris,0)
	}
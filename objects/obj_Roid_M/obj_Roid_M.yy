{
    "id": "a8ab7652-cfe4-4385-8781-ad051cb75cce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Roid_M",
    "eventList": [
        {
            "id": "1646a8cf-2ffe-4172-9fdd-e8ae07c8a3ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a8ab7652-cfe4-4385-8781-ad051cb75cce",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a8ab7652-cfe4-4385-8781-ad051cb75cce"
        },
        {
            "id": "8084576d-5b1b-4b03-8b17-fa7f5a4e3db2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "90da8095-774e-4686-8f2f-b1f5ca2c3255",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a8ab7652-cfe4-4385-8781-ad051cb75cce"
        },
        {
            "id": "17e3c238-39e7-4a62-a5d2-32a7c04f2fad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cb20967-820f-4988-9f08-32ea34ec9e21",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a8ab7652-cfe4-4385-8781-ad051cb75cce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f2191131-ba11-4f84-b804-8934619d17fe",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.8,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "400166bc-d0d5-45c7-88f6-73451b59b696",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 10.9252357,
            "y": 2.47663784
        },
        {
            "id": "e6c1afb4-76f5-46bc-8440-e70f8d2db744",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 20.30841,
            "y": 1.94392776
        },
        {
            "id": "7843a111-d2fd-48a8-9c6b-193271d96a91",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30.4859848,
            "y": 6.04673
        },
        {
            "id": "446f6d9e-015d-4d01-bac3-087f44a6620b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 31.2056046,
            "y": 22.6542053
        },
        {
            "id": "0c9a4476-a3d9-4a5b-a6e0-203ec856adea",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 30.1869125,
            "y": 30.3644867
        },
        {
            "id": "99abdac3-b7f8-429d-99bb-a38b753c8a75",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 32
        },
        {
            "id": "fcd46dfc-9f68-4881-a037-234263cdf8ca",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 23
        },
        {
            "id": "3b5fab76-fac8-4afe-ad81-a423073b22dd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 3,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9231f18-453f-4fc2-ab13-d76ecfe12243",
    "visible": true
}
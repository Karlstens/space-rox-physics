{
    "id": "e2a791f7-e622-4c03-bea2-2801d3254d3d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Debris",
    "eventList": [
        {
            "id": "b5d11d3b-ff92-4bd9-8d88-925ca7c806ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2a791f7-e622-4c03-bea2-2801d3254d3d"
        },
        {
            "id": "d316b8d7-b141-4f5f-9fe8-1e0ca070ff2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "e2a791f7-e622-4c03-bea2-2801d3254d3d"
        },
        {
            "id": "21344d20-1156-4647-bec4-65d303078055",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e2a791f7-e622-4c03-bea2-2801d3254d3d"
        },
        {
            "id": "963c9fee-c9eb-4558-9e6f-f290fdead21a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "e2a791f7-e622-4c03-bea2-2801d3254d3d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.01,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": true,
    "physicsLinearDamping": 0.75,
    "physicsObject": true,
    "physicsRestitution": 0.5,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "0bfef759-a207-4e97-ab93-61dab04a3247",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "fd7769d4-66f3-4c26-943d-1024e5de172c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 0
        },
        {
            "id": "9d85002e-9dca-4d07-ae5a-f27ca19a3289",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 2
        },
        {
            "id": "e97509a1-e178-4394-99d8-c0139c0308d5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 2
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73fa318d-394d-448c-8b99-b0f6f1cebbf3",
    "visible": true
}
{
    "id": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Dynamic",
    "eventList": [
        {
            "id": "1dc39832-c6c1-466a-a14a-1c1c44e26c51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ee3dbe65-49fa-4567-9dfd-6a446869de5b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ee3dbe65-49fa-4567-9dfd-6a446869de5b"
        },
        {
            "id": "95a8b447-e74f-4f42-94e3-5d35e6391a8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ee3dbe65-49fa-4567-9dfd-6a446869de5b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.5,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 100,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "7a0b0d45-f837-482a-a197-deceb9173549",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "bb062e87-94e2-4fda-b180-a7f968894236",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "60f87f65-efc1-4b4d-98b6-f6b508c54779",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "4674936d-72dd-461f-ba82-c06727588228",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": false,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}